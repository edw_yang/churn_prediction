## Datasets are created based on play log data

The CSV files contains the data extracted from play log. 

1. Tagpro5d10d is the game Tagpro with 5 days as OP and 10 days as CP.
2. Tagpro7d7d is with 7 days as OP and 7 days as CP

## Codes are in the jupyter notebook

Here are two notebooks, one is for creating the dataset, the other one is the models buit upon the dataset.

The details of creating dataset are described in notebook.

## Three traditional models (RandomForest, SVM, LogisticRegression) are used

There models get similar AUC score with the models in the paper.

![](AUCs.png)
